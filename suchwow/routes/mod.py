from flask import Blueprint, render_template, redirect, url_for, flash, request

from suchwow.models import AuditEvent, Post, Profile, Moderator, Ban, get_ban_reason
from suchwow.utils.decorators import moderator_required
from suchwow.utils.helpers import get_session_user, audit_event
from suchwow import config


bp = Blueprint("mod", "mod")

@bp.route('/mods')
@moderator_required
def main():
    live_posts = Post.select().where(Post.approved == True).count()
    pending_posts = Post.select().where(Post.approved == False).count()
    active_posters = Profile.select().join(Post, on=Post.submitter == Profile.username).distinct().count()
    mods = Moderator.select().count()
    return render_template(
        'mod/main.html',
        live_posts=live_posts,
        pending_posts=pending_posts,
        active_posters=active_posters,
        mods=mods
    )


@bp.route('/mods/pending')
@moderator_required
def pending_posts():
    posts = Post.select().where(Post.approved == False).order_by(Post.timestamp.asc())
    if not posts:
        flash('no posts pending', 'is-warning')
        return redirect(url_for('mod.main'))
    return render_template('mod/posts.html', posts=posts)


@bp.route('/mods/manage', methods=['GET', 'POST'])
@moderator_required
def manage_mods():
    to_delete = request.args.get('delete')
    if to_delete:
        m = Moderator.select().where(Moderator.username == to_delete).first()
        if not m:
            flash('No moderator exists with that name', 'is-danger')
        elif m.username == get_session_user():
            flash('Cannot remove yourself.', 'is-danger')
        elif m.username == config.SUPER_ADMIN:
            flash('Cannot delete super admin you son-of-a-bitch.', 'is-danger')
        else:
            m.delete_instance()
            audit_event(f'Deleted {to_delete} from mods')
            flash(f'Removed {to_delete} from mods!', 'is-success')
        return redirect(url_for('mod.manage_mods'))
    if request.method == 'POST':
        to_add = request.form.get('username', None)
        if to_add:
            u = Profile.select().where(Profile.username == to_add).first()
            if not u:
                flash('That user does not appear to exist (no profile setup yet)', 'is-danger')
            elif Moderator.select().where(Moderator.username == to_add).first():
                flash(f'{to_add} is already a mod, ya dingus.', 'is-warning')
            else:
                m = Moderator(username=to_add)
                m.save()
                audit_event(f'Added {to_add} to mods')
                flash(f'Added {to_add} to mods!', 'is-success')
    mods = Profile.select().join(Moderator, on=(Profile.username == Moderator.username))
    return render_template('mod/manage.html', mods=mods)


@bp.route('/mods/bans', methods=['GET', 'POST'])
@moderator_required
def manage_bans():
    to_delete = request.args.get('delete')
    if to_delete:
        ban = Ban.select().join(Profile).where(Profile.username == to_delete).first()
        if not ban:
            flash('No ban exists for that user', 'is-danger')
        elif ban.user == get_session_user():
            flash('Cannot ban yourself.', 'is-danger')
        elif ban.user == config.SUPER_ADMIN:
            flash('Cannot ban super admin you son-of-a-bitch.', 'is-danger')
        else:
            ban.delete_instance()
            audit_event(f'Removed ban on {to_delete}')
            flash(f'Unbanned {to_delete}!', 'is-success')
        return redirect(url_for('mod.manage_bans'))
    if request.method == 'POST':
        to_add = request.form.get('username', None)
        if to_add:
            u = Profile.select().where(Profile.username == to_add).first()
            if not u:
                flash('That user does not appear to exist (no profile setup yet)', 'is-danger')
            elif Ban.select().join(Profile).where(Profile.username == to_add).first():
                flash(f'{to_add} is already banned, ya dingus.', 'is-warning')
            elif to_add == config.SUPER_ADMIN:
                flash('Cannot ban the super admin you son-of-a-bitch.', 'is-danger')
            else:
                reason = request.form.get('reason')
                if not reason:
                    reason = get_ban_reason()
                ban = Ban(user=u, reason=reason)
                ban.save()
                audit_event(f'Banned {to_add} ({reason})')
                flash(f'Banned {to_add}!', 'is-success')
    bans = Ban.select()
    return render_template('mod/bans.html', bans=bans)


@bp.route('/mods/logs')
@moderator_required
def view_logs():
    events = AuditEvent.select().order_by(AuditEvent.timestamp.desc()).limit(50)
    return render_template('mod/logs.html', logs=events)