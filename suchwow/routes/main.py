from math import ceil

from flask import Blueprint, request, render_template, flash

from suchwow.models import Post, Profile, Moderator
from suchwow.utils.helpers import get_latest_tipped_posts

bp = Blueprint('main', 'main')


@bp.route("/")
def index():
    itp = 15
    page = request.args.get("page", 1)
    submitter = request.args.get("submitter", None)
    content = request.args.get("content", None)

    if content == 'latest_tipped':
        posts = get_latest_tipped_posts()
        return render_template(
            "index.html",
            posts=posts[0:30],
            title="Latest Tipped Memes"
        )

    try:
        page = int(page)
    except:
        flash("Wow, wtf hackerman. Cool it.", "is-danger")
        page = 1

    posts = Post.select().where(Post.approved==True).order_by(Post.timestamp.desc())
    if submitter:
        posts = posts.where(Post.submitter==submitter)

    paginated_posts = posts.paginate(page, itp)
    total_pages = ceil(posts.count() / itp)
    return render_template(
        "index.html",
        posts=paginated_posts,
        page=page,
        total_pages=total_pages,
        title="Latest Memes"
    )


@bp.route("/about")
def about():
    mods = Profile.select().join(Moderator, on=(Profile.username == Moderator.username))
    return render_template("about.html", mods=mods)