from flask import render_template, Blueprint, request

from suchwow.utils.helpers import get_top_posters, get_top_posts


bp = Blueprint("leaderboard", "leaderboard")

@bp.route("/leaderboards/top_posters")
def top_posters():
    top_posters = get_top_posters()
    return render_template("leaderboard.html", posters=top_posters)

@bp.route("/leaderboards/top_posts")
def top_posts():
    days = request.args.get('days', 1)
    try:
        days = int(days)
    except:
        days = 1

    if days not in [1, 3, 7, 30, 9999]:
        days = 7

    posts = get_top_posts(days)
    return render_template("post/top.html", posts=posts, days=days)
