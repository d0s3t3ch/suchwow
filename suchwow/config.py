from os import getenv
from dotenv import load_dotenv


load_dotenv()

# generated from https://login.wownero.com/developer/register
OIDC_URL = getenv('OIDC_URL', 'https://login.wownero.com/auth/realms/master/protocol/openid-connect')
OIDC_CLIENT_ID = getenv('OIDC_CLIENT_ID', 'suchwow-dev')
OIDC_CLIENT_SECRET = getenv('OIDC_CLIENT_SECRET', '')
OIDC_REDIRECT_URL = getenv('OIDC_REDIRECT_URL', 'http://localhost:5000/auth')

# you specify something
SECRET_KEY = getenv('SECRET_KEY', 'yyyyyyyyyyyyy')       # whatever you want it to be
DATA_FOLDER = getenv('DATA_FOLDER', '/path/to/uploads')  # some stable storage path
SERVER_NAME = getenv('SERVER_NAME', 'localhost')         # name of your DNS resolvable site (.com)
SUPER_ADMIN = getenv('SUPER_ADMIN', 'lza_menace')        # top dawg you cannot delete
WALLET_HOST = getenv('WALLET_HOST', 'localhost')         #
WALLET_PORT = int(getenv('WALLET_PORT', 8888))           #
WALLET_PROTO = getenv('WALLET_PROTO', 'http')            #
WALLET_RPC_USER = getenv('WALLET_RPC_USER', 'suchwow')   #
WALLET_RPC_PASS = getenv('WALLET_RPC_PASS', 'suchwow')   #
WALLET_PASS = getenv('WALLET_PASS', 'zzzzzzz')           # You specify all these wallet details in .env

# Optional for posting to Reddit
PRAW_CLIENT_SECRET = getenv('PRAW_CLIENT_SECRET', None)
PRAW_CLIENT_ID = getenv('PRAW_CLIENT_ID', None)
PRAW_USER_AGENT = getenv('PRAW_USER_AGENT', None)
PRAW_USERNAME = getenv('PRAW_USERNAME', None)
PRAW_PASSWORD = getenv('PRAW_PASSWORD', None)

# Optional for posting to Discord
DISCORD_URL = getenv('DISCORD_URL', None)

# Optional for posting to Mattermost
MM_ICON = getenv('MM_ICON', 'https://funding.wownero.com/static/wowdoge-a.jpg')
MM_CHANNEL = getenv('MM_CHANNEL', 'suchwow')
MM_USERNAME = getenv('MM_USERNAME', 'SuchWow!')
MM_ENDPOINT = getenv('MM_ENDPOINT', 'ppppppppppppppppppppppppp')

# defaults
SESSION_TYPE = 'filesystem'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif', 'svg', 'mp4'}
MAX_CONTENT_LENGTH = 32 * 1024 * 1024
TEMPLATES_AUTO_RELOAD = getenv('TEMPLATES_AUTO_RELOAD', True)
