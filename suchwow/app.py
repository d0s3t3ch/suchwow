from flask import Flask
from flask_session import Session

from suchwow.routes import auth, post, profile, leaderboard, api, mod, main
from suchwow import filters, cli


app = Flask(__name__)
app.config.from_envvar("FLASK_SECRETS")
app.secret_key = app.config["SECRET_KEY"]
Session(app)

app.register_blueprint(post.bp)
app.register_blueprint(auth.bp)
app.register_blueprint(profile.bp)
app.register_blueprint(leaderboard.bp)
app.register_blueprint(api.bp)
app.register_blueprint(mod.bp)
app.register_blueprint(main.bp)
app.register_blueprint(filters.bp)
app.register_blueprint(cli.bp)


if __name__ == "__main__":
    app.run()
