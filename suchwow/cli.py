from os import makedirs

import click
from flask import Blueprint, url_for, current_app

from suchwow.models import Post, Profile, Comment, Notification, db, Moderator, Ban, AuditEvent
from suchwow.utils.helpers import get_latest_tipped_posts
from suchwow.utils.helpers import get_top_posters, get_top_posts
from suchwow.reddit import make_post
from suchwow import wownero
from suchwow import config

bp = Blueprint('cli', 'cli', cli_group=None)


@bp.cli.command("init")
def init():
    # create subdirs
    for i in ["uploads", "db", "wallet"]:
        makedirs(f"{config.DATA_FOLDER}/{i}", exist_ok=True)

    # init db
    db.create_tables([Post, Profile, Comment, Notification, Moderator, Ban, AuditEvent])


@bp.cli.command("post_reddit")
@click.argument('last_hours')
def post_reddit(last_hours):
    posts = Post.select().where(
        Post.approved==True,
        Post.to_reddit==False
    ).order_by(Post.timestamp.asc())
    for p in posts:
        if p.hours_elapsed() < int(last_hours):
            if not p.to_reddit:
                _p = make_post(p)
                if _p:
                    p.to_reddit = True
                    p.save()
                    return


@bp.cli.command("create_accounts")
def create_accounts():
    wallet = wownero.Wallet()
    for post in Post.select():
        if post.account_index not in wallet.accounts():
            account = wallet.new_account()
            print(f"Created account {account}")


@bp.cli.command("payout_users")
def payout_users():
    wallet = wownero.Wallet()
    _fa = wownero.from_atomic
    _aw = wownero.as_wownero
    for post in Post.select():
        try:
            submitter = Profile.get(username=post.submitter)
            balances = wallet.balances(post.account_index)
            url = url_for('post.read', id=post.id, _external=True)
            if balances[1] > 0.05:
                print(f"Post #{post.id} has {balances[1]} funds unlocked and ready to send. Sweeping all funds to user's address ({submitter.address}).")
                sweep = wallet.sweep_all(account=post.account_index, dest_address=submitter.address)
                print(sweep)
                if "tx_hash_list" in sweep:
                    amount = 0
                    for amt in sweep["amount_list"]:
                        amount += int(amt)
        except Exception as e:
            print(f"Failed because: {e}")


@bp.cli.command("show")
@click.argument("post_id")
def post_id(post_id):
    p = Post.filter(id=post_id).first()
    if p:
        print(p.show())
    else:
        print("That post doesn't exist")


@bp.cli.command("load_cache")
def load_cache():
    current_app.logger.info('loading top posters into cache')
    get_top_posters()
    current_app.logger.info('done')
    current_app.logger.info('loading latest tipped into cache')
    get_latest_tipped_posts()
    current_app.logger.info('done')
    for i in [1, 3, 7, 30, 9999]:
        current_app.logger.info(f'loading top posts last {i} days into cache')
        get_top_posts(i)
        current_app.logger.info('done')