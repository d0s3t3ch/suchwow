# SuchWow!

Yo. This is a goofy little CRUD app that interacts with wownero-wallet-rpc to allow people to post memes and get tipped in WOW. It uses [Wownero's SSO & Identity service](https://login.wownero.com/developer/docs/backend) as an authentication backend.

It's lacking a lot of advanced functionality and features....but is pretty rock solid in operation. I rarely have to interact w/ my VPS, it just goes...

It was created haphazardly and drunkenly (at varying times). It (hopefully) stays true to the core ethos of Wownero (I feel) which is, "fuck it, lgtm, ship it". Rough around the edges but overall works great. Was fun to whip together and a blast to see it grow with a community behind it! Thanks all!

## Design

Using the "account" mechanism in the wallets (not subaddresses). Every post gets it's own Wownero account within that running wallet RPC process and is mapped to it. Funds go into the account when tipped. The payout script just checks the balances of each account on an interval and sends funds to the user associated with the artwork associated with said account.

## Setup

There are quite a few prerequisites to run the web service:
* [register](https://login.wownero.com/developer/register) your app on [Wownero SSO](https://login.wownero.com/developer/docs/backend)
* install Wownero binaries/software
* setup secrets and config
* initialize new Wownero wallet and retain seed
* run the wownero-wallet-rpc process
* install local system Python dependencies
* initialize new sqlite db
* setup scheduled tasks to run payouts

I like to ship with Docker, adjust how you'd prefer.

```
# setup secrets in env file outside of git
cp env-example .env

# register on wownero sso
# https://login.wownero.com/developer/register

# inject generated keys into suchwow config
# create new secrets for new wallet, flask server, data, etc
vim .env  

# install docker
sudo apt-get install docker.io docker-compose -y
usermod -aG docker ubuntu
sudo -u ubuntu bash # login w/ new group perms

# run wownero wallets via docker - store seed, ensure rpc running `docker ps`
./run_wallets.sh

# install python dependencies locally
sudo apt-get install python3-venv -y
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

# initialize a new sqlite db
./manage.sh init

# setup recurring payouts
# see crontab.txt for cron syntax example
crontab -e

# run a dev server
./manage.sh run  # access on localhost:5000 - flask local dev, not for prod

# run a prod server
./manage.sh prod # run gunicorn on loopback - pass to nginx reverse proxy

# kill prod server
pkill -e -f gunicorn
```

# Operational Tasks

You'll want to promote a user as a moderator to review posts:

```
./manage.sh add_admin lza_menace
```

or remove them:

```
./manage.sh remove_admin lza_menace
```

Wallets bug out fairly often. Sometimes you might need to punch in a new daemon or restart a wallet:

```
docker stop suchwow-wallet-rpc
docker rm suchwow-wallet-rpc
vim .env
# modify DAEMON_URI
./run_wallets.sh
```

Manually run a payout:

```
./manage.py payout_users
```

There's other shit, but this is the most common. You'll get the hang of it.
