#!/bin/bash

set -e
set +x

# mainnet wownero

export $(cat .env)

if [ ! -d "$WALLET_PATH" ]; then
  # initialize new wallet and retain seed
  docker run --rm -it --name suchwow-wallet-init \
    -v $WALLET_PATH:/root \
    lalanza808/wownero:latest \
    wownero-wallet-cli \
      --daemon-address $DAEMON_URI \
      --generate-new-wallet /root/wow \
      --password $WALLET_PASS
fi

# run rpc process
docker run --restart=always -d --name suchwow-wallet-rpc \
  -v $WALLET_PATH:/root \
  -p 8888:8888 \
  lalanza808/wownero:latest \
  wownero-wallet-rpc \
    --daemon-address $DAEMON_URI \
    --wallet-file /root/wow \
    --password $WALLET_PASS \
    --rpc-bind-port 8888 \
    --rpc-bind-ip 0.0.0.0 \
    --confirm-external-bind \
    --rpc-login "$WALLET_RPC_USER:$WALLET_RPC_PASS" \
    --log-file /root/rpc.log
